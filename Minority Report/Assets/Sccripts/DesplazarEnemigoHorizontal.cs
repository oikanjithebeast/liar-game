﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesplazarEnemigoHorizontal : MonoBehaviour
{
	
	public float minimoenX;
	public float maximoenX;
	public float TiempoEspera = 2f;
	public float Velocidad = 1f;

	private GameObject LugarObjetivo;


	void Start()
    {
		UpdateObjetivo();
		StartCoroutine("Patrullar");
	}

	void Update()
    {
        
    }


	private void UpdateObjetivo()
	{
	
		if (LugarObjetivo == null) {
	LugarObjetivo = new GameObject("Sitio_objetivo");
			LugarObjetivo.transform.position = new Vector2(minimoenX, transform.position.y);
			transform.localScale = new Vector3(-1, 1, 1);
			return;
		}

		if (LugarObjetivo.transform.position.x == minimoenX) {
		LugarObjetivo.transform.position = new Vector2(maximoenX, transform.position.y);
			transform.localScale = new Vector3(1, 1, 1);
		}

		
		else if (LugarObjetivo.transform.position.x == maximoenX) {
		
		
			LugarObjetivo.transform.position = new Vector2(minimoenX, transform.position.y);
			transform.localScale = new Vector3(-1, 1, 1);
		}
	}

	private IEnumerator Patrullar()
	{
	
		while(Vector2.Distance(transform.position, LugarObjetivo.transform.position) > 0.05f) {
			
		
			Vector2 direction = LugarObjetivo.transform.position - transform.position;
			float xDirection = direction.x;

			transform.Translate(direction.normalized * Velocidad * Time.deltaTime);

			yield return null;
		}

	
		transform.position = new Vector2(LugarObjetivo.transform.position.x, transform.position.y);
	
		

		yield return new WaitForSeconds(TiempoEspera);
		UpdateObjetivo();
		StartCoroutine("Patrullar");
	}
}
