using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class areadialogo : MonoBehaviour
{
    public GameObject TextoFlotante;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D c1)
    {
        if (c1.tag == "Player")
        {
            GameObject texto = Instantiate(TextoFlotante);
            texto.transform.position = new Vector3(this.gameObject.transform.position.x,
                this.gameObject.transform.position.y + 4,
                this.gameObject.transform.position.z);
        }
    }
}
