using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontroller : MonoBehaviour
{
    public Transform personaje;

    private float tamaņoCamara;
    private float alturaPantalla;
    // Start is called before the first frame update
    void Start()
    {
        tamaņoCamara = Camera.main.orthographicSize;
        alturaPantalla = tamaņoCamara * 2.5f;

        
    }

    // Update is called once per frame
    void Update()
    {
        CalcularPosicionCamara();
        
    }
    void CalcularPosicionCamara()
    {
        int pantallaPersonaje =(int) (personaje.position.x / alturaPantalla);
        float alturaCamara = (pantallaPersonaje * alturaPantalla) + tamaņoCamara;
        transform.position = new Vector3(alturaCamara,transform.position.y, transform.position.z);
    }
}
